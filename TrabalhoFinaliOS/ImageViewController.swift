//
//  ImageViewController.swift
//  TrabalhoFinaliOS
//
//  Created by Marco Antonio Velloni Figueiredo Junior on 12/17/15.
//  Copyright © 2015 Marco Antonio Velloni Figueiredo Junior. All rights reserved.
//

import UIKit

class ImageViewController: UIViewController, UIGestureRecognizerDelegate {

    
    var arrayImagens : [String]! = []
    var counter : Int = 0
    
    
    @IBOutlet var imageVIew : UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        preencheArray()
        self.imageVIew.userInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: "mudaImagemOnTap:" )
        tapGesture.delegate = self
        self.imageVIew.addGestureRecognizer(tapGesture)
        
        
    }

    override func viewDidAppear(animated: Bool) {
        
        
        imageVIew = UIImageView(image: rotacionaImagens(arrayImagens[counter]))
        self.view.addSubview(imageVIew!)
    }
    
    func preencheArray()
    {
        arrayImagens.append("Images/amiibo1.png")
        arrayImagens.append("Images/amiibo2.png")
        arrayImagens.append("Images/amiibo3.jpg")
        arrayImagens.append("Images/amiibo4.png")
        arrayImagens.append("Images/amiibo5.jpg")
        
    }
    
    func rotacionaImagens(urlImagem: String) -> UIImage!
    {
        let image: UIImage = UIImage(named: urlImagem)!
        
        return image
        
    }
    
    
    func mudaImagemOnTap(sender: UIView!)
    {
        
        if counter < 4
        {
            counter++
            
            imageVIew.image = nil
            imageVIew = UIImageView(image: rotacionaImagens(arrayImagens[counter]))
            self.view.addSubview(imageVIew!)
            
            
        }
        else
        {
            counter = 0
            
            imageVIew.image = nil
            imageVIew = UIImageView(image: rotacionaImagens(arrayImagens[counter]))
            self.view.addSubview(imageVIew!)
        }
    }
    
    

    
    
}
