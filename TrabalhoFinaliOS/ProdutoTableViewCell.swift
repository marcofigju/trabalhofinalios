//
//  ProdutoTableViewCell.swift
//  TrabalhoFinaliOS
//
//  Created by Marco Antonio Velloni Figueiredo Junior on 12/17/15.
//  Copyright © 2015 Marco Antonio Velloni Figueiredo Junior. All rights reserved.
//

import UIKit

class ProdutoTableViewCell: UITableViewCell {

    @IBOutlet var labelCell : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
