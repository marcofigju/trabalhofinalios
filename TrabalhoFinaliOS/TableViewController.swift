//
//  TableViewController.swift
//  TrabalhoFinaliOS
//
//  Created by Marco Antonio Velloni Figueiredo Junior on 12/17/15.
//  Copyright © 2015 Marco Antonio Velloni Figueiredo Junior. All rights reserved.
//

import UIKit

class TableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var arrayTrazido : [String]! = []
    
    @IBOutlet var tableView : UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.registerNib(UINib(nibName: "ProdutoTableViewCell", bundle: nil), forCellReuseIdentifier: "ProdutoTableViewCell")
        
        
        
        if let testArray : AnyObject? = NSUserDefaults.standardUserDefaults().objectForKey("array") {
            let readArray : [String] = testArray! as! [String]
            arrayTrazido = readArray
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayTrazido.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell : ProdutoTableViewCell = tableView.dequeueReusableCellWithIdentifier("ProdutoTableViewCell", forIndexPath: indexPath) as! ProdutoTableViewCell
        
        cell.labelCell.text = arrayTrazido[indexPath.row]
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let vc: ProdutoDetalheViewController = ProdutoDetalheViewController(nibName: "ProdutoDetalheViewController", bundle: nil)
        vc.labelString = arrayTrazido[indexPath.row]
        
        self.navigationController?.presentViewController(vc, animated: true, completion: nil)
    }


}
