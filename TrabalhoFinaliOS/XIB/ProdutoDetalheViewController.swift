//
//  ProdutoDetalheViewController.swift
//  TrabalhoFinaliOS
//
//  Created by Marco Antonio Velloni Figueiredo Junior on 12/17/15.
//  Copyright © 2015 Marco Antonio Velloni Figueiredo Junior. All rights reserved.
//

import UIKit

class ProdutoDetalheViewController: UIViewController {
    
    
    @IBOutlet var label : UILabel!

    @IBAction func voltar(){
        let controllerAtual :UIViewController! = self.presentingViewController;
        
        self.dismissViewControllerAnimated(false, completion: {()->Void in
            print("done");
            controllerAtual.dismissViewControllerAnimated(false, completion: nil)
        })
    }
    
    var labelString : String!
    override func viewDidLoad() {
        super.viewDidLoad()

        label.text = labelString
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
