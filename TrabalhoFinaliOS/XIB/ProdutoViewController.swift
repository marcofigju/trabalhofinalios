//
//  ProdutoViewController.swift
//  TrabalhoFinaliOS
//
//  Created by Marco Antonio Velloni Figueiredo Junior on 12/17/15.
//  Copyright © 2015 Marco Antonio Velloni Figueiredo Junior. All rights reserved.
//

import UIKit

class ProdutoViewController: UIViewController {
    
    
    @IBOutlet var textField     : UITextField!
    
    var arrayText : [String]! = []
    
    
    
    @IBAction func salvaNoArray()
    {
        arrayText.append(textField.text!)
        textField.text = ""
        
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(arrayText, forKey: "array")
        
        print(arrayText.last)
        
        let controllerAtual :UIViewController! = self.presentingViewController;
        
        self.dismissViewControllerAnimated(false, completion: {()->Void in
            print("done");
            controllerAtual.dismissViewControllerAnimated(false, completion: nil)
        })
        
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        if let testArray : AnyObject? = NSUserDefaults.standardUserDefaults().objectForKey("array") {
            if let readArray : [String] = testArray! as? [String] {
                arrayText = readArray
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}
